#include "co_cpu.h"


//template<sc_uint<7> addr>
void co_cpu<addr>::start_stop_process() {
  
  sda_reg.write( sda.read() );
 
  scl_reg.write( scl.read() );
 
  scl_rising.write( SC_LOGIC_0 );
 
  if ((scl.read() == SC_LOGIC_1) && (scl_reg.read() == SC_LOGIC_0)) {
    scl_rising.write( SC_LOGIC_1 );
   } 

  scl_falling.write( SC_LOGIC_0 );
 
  if (((scl.read() == SC_LOGIC_0) && (scl_reg.read() == SC_LOGIC_1))) {
    scl_falling.write(SC_LOGIC_1);
  } 

  start_reg.write(SC_LOGIC_0);
 
  stop_reg.write(SC_LOGIC_0);
 
  if (((((scl.read() == SC_LOGIC_1) && (scl_reg.read() == SC_LOGIC_1)) && (sda.read() == SC_LOGIC_0)) && (sda_reg.read() == SC_LOGIC_1))) {
  
    start_reg.write(SC_LOGIC_1);
 
    stop_reg.write(SC_LOGIC_0);
   

  } 
  if (((((scl.read() == SC_LOGIC_1) && (scl_reg.read() == SC_LOGIC_1)) && (sda.read() == SC_LOGIC_1)) && (sda_reg.read() == SC_LOGIC_0))) {
  
  stop_reg.write(SC_LOGIC_1 );
 
  start_reg.write(SC_LOGIC_0);
   
  }   
}

template<sc_uint<7> addr>
void co_cpu<addr>::slave_fsm() {
  if (i2c_rst.read() == SC_LOGIC_1) {
    i2c_st.write((i2c_state_t)(start ));
    i2c_fail.write(SC_LOGIC_0 );
    c.write(0 );
    r.write(0 );
  } else if (clk.posedge()) {
    switch (i2c_st.read()) {
      case start :
        if ((start_reg.read() == SC_LOGIC_1)) {
          i2c_st.write( (i2c_state_t)(data_wr ) );
          c.write(0 );
          continue_reg.write(SC_LOGIC_1 );
        } 
        break;
  
      case data_wr :
        if (scl_rising.read() == SC_LOGIC_1) {
          if (c.read() <= 7) {
            i2c_slave_reg[r.read() ][(7 - c.read()) ] = sda.read() ;
            c.write((c.read() + SC_LOGIC_1) );
          } 
          if (c.read() == 7) {
            //data valid and done
            
          }   

        } 
        if ((scl_falling.read() == SC_LOGIC_1) && (c.read() == 8)) {
          i2c_st.write((i2c_state_t)(rw_ack ));
          c.write(0 );
        } 
        break;
 
      case rw_ack :
        if (scl_falling.read() == SC_LOGIC_1) {
          if (continue_reg.read() == SC_LOGIC_1) {
            if (rw_reg.read() == SC_LOGIC_0) {
              i2c_st.write((i2c_state_t)(data_rd ));
            } else {
              i2c_st.write((i2c_state_t)(data_wr ));
              r.write((r.read() + 1) ); 
            }  
          } else {
            i2c_st.write((i2c_state_t)(stop ));
          }   
        }
        break;

      case stop :
        break;
 
      default : 
        SC_REPORT_FATAL("?","I2C slave in incorrect state");
        break;
    } 

    if (start_reg.read() == SC_LOGIC_1) {
      i2c_st.write((i2c_state_t)(data_wr ));
      c.write(0 );
    } 
    if (stop_reg.read() == SC_LOGIC_1) {
      i2c_st.write((i2c_state_t)(start ));
      c.write(0);
    } 
    if (i2c_rst.read() == SC_LOGIC_1) {
      i2c_st.write((i2c_state_t)(start ));
    }   
  }
}
template<sc_uint<7> addr>
void co_cpu<addr>::comb_assignments() {

  i2c_rst.write( reset.read() );
  sda.write(((sda_wen.read() == SC_LOGIC_1)) ? sda_o.read()  : SC_LOGIC_Z );
  scl.write(((scl_wen.read() == SC_LOGIC_1)) ? scl_o.read()  : SC_LOGIC_Z );

}

