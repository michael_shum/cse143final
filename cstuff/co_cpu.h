#ifndef CO_CPU_H
#define CO_CPU_H

#include <systemc.h>

//??
template<sc_lv<7> addr>

SC_MODULE(co_cpu) {


    sc_in<sc_logic >  clk;
    sc_in<sc_logic >  reset;
    sc_inout<sc_logic > scl;
    sc_inout<sc_logic > sda;

typedef sc_lv<8> regfile;

//state datatype
enum i2c_state_t {start  ,command ,command_ack ,data_wr ,data_rd ,data_rd_ack ,rw_ack ,stop};

sc_signal<sc_logic > i2c_rst,i2c_fail; 
sc_signal<i2c_state_t > i2c_st; // := START
sc_signal<regfile > i2c_slave_reg[16]; // lv<8>[16]
sc_signal<sc_logic > rw_reg;
sc_signal<sc_logic > continue_reg;
sc_signal<sc_logic > sda_o,scl_o,sda_wen,scl_wen; // := '0'
sc_signal<sc_logic > sda_reg;
sc_signal<sc_logic > scl_reg;
sc_signal<sc_logic > start_reg;
sc_signal<sc_logic > stop_reg;
sc_signal<sc_logic > scl_falling;
sc_signal<sc_logic > scl_rising;
sc_signal<int > c; // natural range 0 to 8,
sc_signal<int > r; // natural range 0 to 15

void start_stop_process();
void slave_fsm();
void comb_assignments();

SC_CTOR(co_cpu) {
  SC_METHOD(start_stop_process);
    sensitive << clk.pos();

  SC_METHOD(slave_fsm);
    sensitive << i2c_rst << i2c_st << c << r << scl_rising << scl_falling << i2c_slave_reg << clk;

  SC_METHOD(comb_assign);
    sensitive << reset << sda_wen << sda_o << scl_wen << scl_o;
}



~co_cpu() {

}

// ??
SC_HAS_PROCESS(co_cpu);

};
#endif
