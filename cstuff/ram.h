#ifndef RAM_H
#define RAM_H

#include <systemc.h>

namespace ram {
const int width=8;
const int depth=2 # 2;

typedef sc_biguint<width - 1+1> ram_type;
}
#endif
