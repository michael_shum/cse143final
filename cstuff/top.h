#ifndef TOP_H
#define TOP_H

#include <systemc.h>
#include "ram.h"

using namespace ram;

#include "cpu.h"
#include "co_cpu.h"
SC_MODULE(top) {
private:
const string matrixbfile;
const int matrixboffset;

public:

sc_signal<sc_logic > clk_t,rst_t;
sc_signal<sc_logic > sda_t,scl_t;
sc_signal<ram_type > ram_t;
void clock();
void init();
void comb_assignments();
cpu cpu0;
co_cpu<"0b0000011" > mmulti;

top(sc_module_name _n) : sc_module(_n),matrixbfile("./matrixb.dat"),matrixboffset(0),cpu0("cpu0"),mmulti("mmulti")
 {
cpu0.clk(clk_t);
cpu0.rst(rst_t);
cpu0.ram(ram_t);
cpu0.sda(sda_t);
cpu0.scl(scl_t);
mmulti.clk(clk_t);
mmulti.reset(rst_t);
mmulti.scl(scl_t);
mmulti.sda(sda_t);

SC_THREAD(init);
sensitive << ();
SC_METHOD(comb_assignments);
sensitive;
}

~top() {
}
SC_HAS_PROCESS(top);

};
#endif
