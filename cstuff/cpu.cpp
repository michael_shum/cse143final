#include "cpu.h"


void cpu::aclk() {
  int count; // nat range 0-31

  
  if (rst.read() == SC_LOGIC_1) {
    i2c_aclk.write( SC_LOGIC_0 );
    count = 0;
  } else if (clk.posedge()) {
     count = (count + 1);
 
    if (count == 31) {
      i2c_aclk.write(~i2c_aclk.read()); // invert i2c_aclk
      count = 0;
    } // end count == 31 if   
  } // end posedge/rst.read if
} 

void cpu::d_b_clks() {
  int count; // nat range 0-4

  if (rst.read() == SC_LOGIC_1) {
  count = 0;
 i2c_bclk.write( SC_LOGIC_0 );
 i2c_dclk.write( SC_LOGIC_0 );
  } else 
if (i2c_aclk.posedge()) {
  count = (count + 1);
 if (count == 1) {
  i2c_bclk.write( SC_LOGIC_0);
  } else 
if (count == 2) {
  i2c_dclk.write(SC_LOGIC_1);
  } else 
if (count == 3) {
  i2c_bclk.write( SC_LOGIC_0);
  } else {
 i2c_dclk.write( SC_LOGIC_0);
 count = 0;

 
}   
}
}
}
void cpu::i2c_fsm_seq() {
  if (i2c_rst.read() == SC_LOGIC_1) {
  i2c_st.write((i2c_state_t)(idle ));
 i2c_fail.write( SC_LOGIC_0);
 c.write((int)(0 ));
 r.write((int)(0 ));
  } else 
if (i2c_dclk.posedge()) {
  if (c.read() == c_match.read()) {
  c.write((int)(0 ));
 i2c_st.write((i2c_state_t)(i2c_n_st.read() ));
 if (i2c_st.read() == data_wr_ack) {
  r.write((r.read() + 1) );
 if (sda.read() == SC_LOGIC_1) {
  i2c_fail.write(SC_LOGIC_1);
  } else {
 i2c_fail.write(SC_LOGIC_0);

 
}  } else 
if (i2c_st.read() == idle) {
  r.write((int)(0 ));
   
}  } else {
 c.write((c.read() + 1) );

 
}  } else 
if (i2c_dclk.negedge()) {
    
}
}
void cpu::i2c_fsm_comb() {
  
switch (i2c_st.read()) {
case idle :
 i2c_rdy.write( SC_LOGIC_1);
 scl.write( SC_LOGIC_0);
 sda.write( SC_LOGIC_0);
 c_match.write((int)(0 ));
 if (i2c_go.read() == SC_LOGIC_1) {
  i2c_n_st.write((i2c_state_t)(start ));
   
} break;
  case start :
 i2c_rdy.write( SC_LOGIC_0);
 scl.write( SC_LOGIC_0);
 sda.write( SC_LOGIC_0);
 c_match.write((int)(0 ));
 if (i2c_r_or_w.read() == SC_LOGIC_1) {
  i2c_n_st.write((i2c_state_t)(idle ));
  } else {
 i2c_n_st.write((i2c_state_t)(data_wr ));

 
} break;
 case data_wr :
 i2c_rdy.write( SC_LOGIC_0);
 scl.write(i2c_bclk.read() );
 sda.write(i2c_reg[r.read() ][7 - c.read() ] );
 c_match.write((int)(7 ));
 i2c_n_st.write((i2c_state_t)(data_wr_ack ));
 break;
 case data_wr_ack :
 i2c_rdy.write(SC_LOGIC_0);
 scl.write((i2c_bclk.read() ));
 sda.write(SC_LOGIC_0 );
 c_match.write((int)(0 ));
 if (r.read() == (i2c_data_len.read() - 1)) {
  i2c_n_st.write((i2c_state_t)(stop ));
  } else {
 i2c_n_st.write((i2c_state_t)(data_wr ));

 
} break;
 case stop :
 i2c_rdy.write( SC_LOGIC_0);
 scl.write( SC_LOGIC_0);
 sda.write(SC_LOGIC_0);
 c_match.write((int)(0 ));
 i2c_n_st.write((i2c_state_t)(idle ));
 break;
 default : SC_REPORT_FATAL("","cpu in a weird state");   break;

}
}
void cpu::program_exe() {
ram_type ram_sliced;

for(;;){
ram_sliced=ram.read();
  wait();
 i2c_r_or_w.write(SC_LOGIC_0);
 i2c_go.write(SC_LOGIC_0);
 i2c_data_len.write((int)(0 ));

 //TODO
**FAILED** {
  i2c_reg[idx ] = "0b00000000" ;
 
} wait();
 ram_sliced[0 ]=(ram_type)("0b00101010" );
 wait();
 ram_sliced[1 ]=(ram_type)("0b00110011" );
 wait();
 wait();
 i2c_r_or_w.write(SC_LOGIC_0);
 wait();
 i2c_data_len.write((int)(5 ));
 wait();
 i2c_reg[0 ] = "0b00000010" ;
 wait();
 i2c_reg[1 ] = "0b01010101" ;
 wait();
 i2c_reg[2 ] = "0b10100101" ;
 wait();
 i2c_reg[3 ] = ram.read()[1 ] ;
 wait();
 i2c_reg[4 ] = ram.read()[0 ] ;
 wait();
 i2c_go.write(SC_LOGIC_1);
 wait();
 i2c_go.write(SC_LOGIC_0);
 wait();
 
 //TODO
wait(); // >>>> FIX ME <<<< wait for 1 us
 stop[1 ];ram.write(ram_sliced);

}
}
void cpu::comb_assignments() {


if (!(i2c_fail.read() == SC_LOGIC_0)) 
  SC_REPORT_WARNING("","ACK NOT RECV, RESETTING I2C");

i2c_rst.write(rst.read() );

if (!(i2c_fail.read() == SC_LOGIC_0)) 
  SC_REPORT_WARNING("","ACK NOT RECV, RESETTING I2C");
}

