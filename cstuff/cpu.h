#ifndef CPU_H
#define CPU_H

#include <systemc.h>
#include "ram.h"

using namespace ram;

SC_MODULE(cpu) {
private:
  const int matrixboffset;
  const string newimagefile;
  const int newimgoffset;
  const int i2ccmdoffset;

public:

  //ports
  sc_in<sc_logic >  clk;
  sc_in<sc_logic >  rst;
  sc_inout<ram_type > ram;
  sc_inout<sc_logic > sda;
  sc_out<sc_logic > scl;

  // states datatype
  enum i2c_state_t {idle  ,start ,command ,addr_ack ,data_wr ,
                      data_rd ,data_wr_ack ,data_rd_ack ,stop};
  //reg typedef
  typedef sc_lv<8> regfile;
  //signals!!!!

  sc_signal<sc_logic > i2c_aclk,i2c_dclk,i2c_bclk;
  sc_signal<sc_logic > i2c_go,i2c_r_or_w,i2c_rdy,i2c_fail,i2c_rst;
  sc_signal<int > i2c_data_len; // natural 0 to 15
  sc_signal<regfile > i2c_reg[16]; 
  sc_signal<i2c_state_t > i2c_st,i2c_n_st; // := IDLE
  sc_signal<sc_logic > i2c_wrflag,i2c_rdflag;
  sc_signal<sc_lv<3> > i2c_ack;
  sc_signal<int > c; // natural range 0 to 7
  sc_signal<int > c_match; // natural range 0 to 7
  sc_signal<int > r; // nat range 0 to 15
  
  //methods
  void aclk();
  void d_b_clks();
  void i2c_fsm_seq();
  void i2c_fsm_comb();
  void program_exe();
  void comb_assignments();
  
  
  //CSTR/init
  cpu(sc_module_name _n) : sc_module(_n), matrixboffset(0), 
    newimagefile("./image.dat"), newimgoffset(1920 * 270),
    i2ccmdoffset((1080 * 1920) + (1920 * 270))
  {
    //throwin in dat sensitivity list
    SC_THREAD(aclk);
      sensitive << clk  << rst;
    SC_THREAD(d_b_clks);
      sensitive << i2c_aclk  << rst;
    SC_METHOD(i2c_fsm_seq);
      sensitive << i2c_dclk  << i2c_rst;
    SC_METHOD(i2c_fsm_comb);
      sensitive << i2c_st  << i2c_bclk << i2c_reg[16] << i2c_go 
                  << i2c_r_or_w << r << c;
    SC_THREAD(program_exe);
      sensitive << rst.pos();
    SC_METHOD(comb_assignments);
      sensitive << rst << i2c_fail;
  }

  ~cpu() 
  {
  }
  
  SC_HAS_PROCESS(cpu);

};
#endif
