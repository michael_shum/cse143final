#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#include <fstream>


#define P 100
using namespace std;

int main(int argc, const char* argv[])
{
  if( argc != 3 )
  {
    printf("incorrect arguments! usage is\n%s in_file out_file\n", argv[0] ); 
    return 666;
  }
  
  //int p = std::stoi(argv[3]);
  // get file
  unsigned char m1[1080][1920]; 
  unsigned char m2[1920][P];
  //memset( m2, 0, 1920*P*sizeof(unsigned char));

  int out[1080][P] = {0}; // in case multiplication is larger than 256
  //memset(out, 0, 1080*p*sizeof(int));
  //assumes input file is 1080x1920 size
  ifstream inF;
  inF.open(argv[1], ifstream::in | ifstream::binary);
  
    
  // load file into memory
  for(int i = 0; i < 1080; i++)
  {
    for(int j = 0; j < 1920; j++)
    {
      m1[i][j] = (unsigned char) inF.get(); 
    }
  }
  
  // where does the second matrix get its data from?
  for(int i = 0; i < 1920; i++)
  {
    for(int j = 0; j < P; j++)
    {
      m2[i][j] = ((i+1)*(j+1)) % 256; // make up random data
    }
  }

  // do matrix multiplication
  
  for(int i = 0; i < 1080; i++)
  {
    for( int j = 0; j < P; j++)
    {
      for( int in = 0; in < (1920-1); in++)
      {
        out[i][j] += m1[i][in] * m2[in][j];
      }
    }
  }
  // save file... 
  ofstream outF;
  outF.open(argv[2], ofstream::out | ofstream::binary);
  for(int i = 0; i < 1080; i++)
  {
    for(int j = 0; j < P; j++)
    {
      outF.put(out[i][j]);
    }
  }

  inF.close();
  outF.close();

  return 1337;
}
