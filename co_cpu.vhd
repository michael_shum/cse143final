library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use std.textio.all;
use std.env.all;

entity co_cpu is
  generic (
    ADDR: std_logic_vector(7 downto 0));
  port( clk:  in std_logic;
        reset:  in std_logic;
        scl:  inout std_logic;
        sda:  inout std_logic;
        INT:  out std_logic

    --    data_o: std_logic_vector(7 downto 0);
    --    data_i: std_logic_vector(7 downto 0);
  );
end entity;

architecture coprocessor of co_cpu is
  constant MY_ADDR: std_logic_vector(7 downto 0) := "00000010";
  type regfile is array (0 to 15) of std_logic_vector(7 downto 0);
  type i2c_state_t is (IDLE,START,COMMAND,COMMAND_ACK,DATA_WR,DATA_RD,DATA_RD_ACK,RW_ACK,STOP);
  signal I2C_RST, I2C_FAIL: std_logic;
  signal I2C_ST: i2c_state_t := IDLE;
  signal I2C_SLAVE_REG: regfile;

  signal dev_addr_reg: std_logic_vector(7 downto 0) := "00000000";
  --signal data_o_reg, data_i_reg: std_logic_vector(7 downto 0) := "00000000";
  signal data_o_reg: std_logic_vector(7 downto 0) := "01010101";
  signal rw_reg: std_logic;
  signal continue_reg, addr_corr: std_logic;
  --signal data_vald: std_logic;

  signal sda_o, scl_o, sda_wen, scl_wen: std_logic := '0';

  signal sda_reg, scl_reg, start_reg, stop_reg, scl_falling, scl_rising: std_logic := 'H';
--  signal scl_reg: std_logic;
--  signal start_reg: std_logic;
--  signal stop_reg: std_logic;
--  signal scl_falling: std_logic;
--  signal scl_rising: std_logic;

  signal c: natural range 0 to 9;
  signal r: natural range 0 to 15;
begin
  -- This is the emulator for the CPU in this architecture. You can think of
  -- this as running a program that is tracking an object from an HD video
  -- camera feed using the KLT algorithm. The only part that you will show
  -- is where the program offloads a matrix multiplication to the co-processor
  start_stop_process: process(clk)
  begin
    if (clk = '1' and clk'event) then
      sda_reg <= sda;
      scl_reg <= scl;

      scl_rising <= '0';
      if scl = 'H' and scl_reg = '0' then
        scl_rising <= '1';
      end if;

      scl_falling <= '0';
      if (scl = '0' and scl_reg = 'H') then
        scl_falling <= '1';
      end if;

      start_reg <= '0';
      stop_reg <= '0';
      if (scl = 'H' and scl_reg = 'H'and sda = '0' and sda_reg = 'H') then
        start_reg <= '1';
        stop_reg <= '0';
      end if;

      if (scl = 'H' and scl_reg = 'H' and sda = 'H' and sda_reg = '0') then
        stop_reg <= '1';
        start_reg <= '0';
      end if;
    end if;
  end process;

  I2C_RST <= reset;
  SLAVE_FSM: process (clk, I2C_RST, I2C_ST, c, scl_rising, scl_falling, r, I2C_SLAVE_REG) 
  begin
    if I2C_RST='1' then
      I2C_ST <= IDLE;
      I2C_FAIL <= '0';
      c <= 0;
      r <= 0;
      INT <= '0';
    elsif clk = '1' and clk'event then
      sda_o <= '0';
      sda_wen <= '0';
      case I2C_ST is
        when IDLE =>
          I2C_ST <= START;
          INT <= '0';

        when START => -- i think this just just a reset state? idk...
          if (scl_falling = '1') then
            c <= 0;
            continue_reg <= '1';
            if (addr_corr = '1') then
              I2C_ST <= DATA_RD;
            else
              I2C_ST <= COMMAND;
            end if;
          end if;

        when COMMAND =>
          if scl_rising = '1' then
            if c < 8 then
              dev_addr_reg(7-c) <= sda;
            elsif c = 8 then
              rw_reg <= sda;
            end if;
            c <= c + 1;
          end if;

          if scl_falling = '1' then
            if c = 9 then
              c <= 0;
              if conv_integer(dev_addr_reg) = conv_integer(MY_ADDR) then
                addr_corr <= '1';
                I2C_ST <= COMMAND_ACK;
              else
                assert false
                report ("addr not equal")
                severity note;
                I2C_ST <= START;
              end if;
            end if;
          end if;

        WHEN COMMAND_ACK =>
          sda_wen <= '1';
          sda_o <= '0';
          if scl_falling = '1' then
            if rw_reg = 'H' then
              I2C_ST <= DATA_WR;
              I2C_SLAVE_REG(r)(7-c) <= SDA;
              c <= c + 1;
            else
              I2C_ST <= DATA_RD;
            end if;
          end if;

        when DATA_WR =>
          if scl_rising = '1' then
            if c <= 7 then
              I2C_SLAVE_REG(r)(7-c) <= SDA;
              c <= c+1;
            end if;
            if c = 7 then
              --data_valid <= '1';
            end if;
          end if;

          if scl_falling = '1' and c = 8 then
            I2C_ST <= RW_ACK; -- TODO: CHANGE TO RW_ACK
            c <= 0;
          end if;

        when DATA_RD =>
          sda_wen <= '1';
          sda_o <= data_o_reg(7-c);
          if scl_falling = '1' then
            if c < 7 then
              c <= c + 1;
            elsif c = 7 then
              I2C_ST <= DATA_RD_ACK;
              c <= 0;
            end if;
          end if;

        when DATA_RD_ACK =>
          if scl_rising = '1' then
            I2C_ST <= RW_ACK;
            if sda = '1' then -- stop read
              continue_reg <= '0';
            else
              continue_reg <= '1';
              --data_i_reg <= data_i;
            end if;
          end if;

        when RW_ACK =>
          if scl_falling = '1' then
            if continue_reg =  '1' then
              if rw_reg = 'H' then
                I2C_ST <= DATA_WR;
                r <= r + 1;
              else
                I2C_ST <= DATA_RD;
              end if;
            else I2C_ST <= STOP;
            end if;
          end if;

        when STOP =>
          null;

        when others =>
          assert false
            report ("I2C slave in incorrect state");
          null;
      end case;

      if start_reg = '1' then
        I2C_ST <= IDLE;
        c <= 0;
      end if;
      
      if stop_reg = '1' then
        I2C_ST <= IDLE;
        INT <= '1';
        c <= 0;
      end if;

      if I2C_RST = '1' then
        I2C_ST <= IDLE;
      end if;
    end if;
  end process;

  sda <= sda_o when sda_wen = '1' else 'Z';
  scl <= scl_o when scl_wen = '1' else 'Z';


end coprocessor;
